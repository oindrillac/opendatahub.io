---
layout: docs
title: Release Notes
permalink: /docs/roadmap/release-notes
style_class: release-notes
---

### Open Data Hub version v0.4.0 - September 2019

| Technology | Version | Category |
|--|--|--|
| Open Data Hub Operator | 0.4.0 | Meta Operator Application management |
| [Argo](https://argoproj.github.io/argo/) | 2.3.0 | Container native workflow engine |
| [Strimzi Kafka Operator](https://strimzi.io/) | 0.11.1 | Distributed streaming platform |
| Open Data Hub AI-Library | 1.0  | Machine learning as a service |
{:class="table table-bordered"}


### Open Data Hub version v0.3.0 - June 2019

|Technology|Version|Category|
|---|---|---|
|Seldon|0.2.7|Model Serving and Metrics Tool|
|JupyterHub with GPU Support|3.0.7|Data science tools|
|Apache Spark|2.2.3|Query & ETL frameworks|
|TwoSigma BeakerX Integration|1.4.0|Data science tools|
|Prometheus|2.3|System monitoring tools|
|Grafana|4.7|System monitoring tools|
{:class="table table-bordered"}


### Open Data Hub version v0.2.0 - May 2019

|Technology|Version|Category|
|---|---|---|
|JupyterHub with GPU Support|3.0.7|Data science tools|
|Apache Spark|2.2.3|Query & ETL frameworks|
|TwoSigma BeakerX Integration|1.4.0|Data science tools|
{:class="table table-bordered"}

{% include next-link.html label="Additional Resources" url="/docs/additional.html" %}
