---
layout: docs
title: Additional Resources
permalink: /docs/additional
---

## Community
- [Gitlab]({{site.repo}})
- [Mailing List]({{ site.email_list }})
<!--- Slack-->
<!--- Meetings-->


## Videos
##### Presentations
- [AI on OpenShift](https://www.youtube.com/watch?v=MD1x2IT7rdg)
- [Fraud Detection using the Open Data Hub](https://youtu.be/IcQ2bhsw_kQ)

##### Conference Talks
- [ML Pipelines with Kubeflow, Argo and Open Data Hub](https://youtu.be/NZOky2Gm0iA?list=PLU1vS0speL2bxDVhBGZOiNQotzkdxJ8ln)
- [Scalable Kafka Deployment on OpenShift for ML](https://youtu.be/og_Abr9jZJU?list=PLU1vS0speL2bxDVhBGZOiNQotzkdxJ8ln)
- [MLFlow: Experiment Tracking on OpenShift](https://youtu.be/WgEKfAj7PLc?list=PLU1vS0speL2bxDVhBGZOiNQotzkdxJ8ln)
- [Scaling your Open Data Hub for Fun and Production](https://youtu.be/dkuTaxWUrfE?list=PLU1vS0speL2bxDVhBGZOiNQotzkdxJ8ln)
- [An Introduction to Unsupervised Deep Learning](https://youtu.be/tpDV8nUv45c?list=PLU1vS0speL2bxDVhBGZOiNQotzkdxJ8ln)
- [Unsupervised NLP for Log Anomaly Detection](https://youtu.be/Dt81qwza-zA?list=PLU1vS0speL2bxDVhBGZOiNQotzkdxJ8ln)
- [Sentiment Analysis Service in a DevOps Environment](https://youtu.be/2QJ367chSS0?list=PLU1vS0speL2bxDVhBGZOiNQotzkdxJ8ln)
- [Machine Learning with Open Source Infrastructure](https://youtu.be/K8G_0z5jbcA?list=PLU1vS0speL2bxDVhBGZOiNQotzkdxJ8ln)
- [AIOps: Anomaly Detection with Prometheus and Istio](https://youtu.be/5lT-GajT_Wo?list=PLU1vS0speL2bxDVhBGZOiNQotzkdxJ8ln)
- [Presto: Cloud Native SQL-on-Anything](https://youtu.be/73VZaP3Mh-M?list=PLU1vS0speL2bxDVhBGZOiNQotzkdxJ8ln)
- [Data Science in the Open Cloud Exchange Model](https://youtu.be/KWDUkm1ZeKY?list=PLU1vS0speL2bxDVhBGZOiNQotzkdxJ8ln)
- [Ceph Object Storage for AI and ML Workloads](https://www.youtube.com/watch?v=n2IW3VIZmg4)
- [Data Exploration with JupyterHub on OpenShift](https://www.youtube.com/watch?v=by0l3b55i7g)
- [Building AI with Ceph and OpenShift](https://www.youtube.com/watch?v=B6E7SyxOB2M)
- [Using the Massachusetts Open Cloud Data Hub to perform Data Science Experiments](https://www.youtube.com/watch?v=iUJ6RGfY0JQ)
- [Using the Mass Open Cloud to perform Data Science Experiments](https://youtu.be/CZwUCgkKIc4)

#### Tutorials
- [Uploading data to Ceph via the command line](https://www.youtube.com/watch?v=d6X1xvDXewM&feature=youtu.be)

## Audio
- [Innovate @Open podcast](https://grhpodcasts.s3.amazonaws.com/opendatahub1908.mp3)
